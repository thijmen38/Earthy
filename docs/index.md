# RAHA

Welcome to RAHA!

<!-- ![Entrance.jpg](img/Entrance.jpg) -->

This is a place of rest, comfort and peace of mind

## About

Raha is our submission for TU Delfts Earthy course. Which is a project that foccusses on designing and building earthy structures with the use of computational programs.
Houw we came to this design is specified on the [design page](https://design.nl).

---

### The Team

The Raha project group consists of 5 members, each member has written a summary of their professional experience below.

**Anagha Yoganand** [Linkedin](https://www.linkedin.com/in/anagha-yoganand/)

nice text about yourself

**Bezawit Z. Bekele** [Linkedin](https://www.linkedin.com/in/bezawit-zerayacob-bekele/)

again nice text about yourself

**Shriya Balakrishnan** [Linkedin](https://www.linkedin.com/in/shriyabalakrishnan/)

and again

**Thijmen Pluimers** [Linkedin](https://www.linkedin.com/in/thijmenpluimers/)

After doing his Bachelor at the Saxion hoge school in Enschede Thijmen decided to come to Delft to enroll in the Building Technology master track.
Having focused his Bachelor graduation on structural engineering he was determined to focus on broadening his knowledge about the building industry during his master study. previous to earthy he already gained knowledge about facades by following the Facade Design course and being a Facade designer in the interdisciplinary course MEGA. It was in this course where he saw the potential of computational design and wanted to learn about this. This naturally brought him to the earthy course.

**Twinkle Nathani** [Linkedin](https://www.linkedin.com/in/twinkle-nathani-88036a105/)

and again
