## 0_Midterm presentation

Thursday the 1st of October 2020 the mid term was presented.
The presentation contained our view on the configuration and form of the project.

<!-- [Click here to download the midterm presentation](rev/presentations/midtermpresentation.pdf) -->

---

## 1_Final presentation

Thursday the 29th of October the final presentation of the project was given.
In this presentation all our findings were presented.

<!-- [Click here to download the end presentation](rev/presentations/endpresentation.pdf) -->

---

## 2_Video
