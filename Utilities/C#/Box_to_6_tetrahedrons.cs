private void RunScript(Box B, object y, ref object A, ref object C)
  {
    //    tetrahedra.Add(new Tetrahedron(ABox, new int[] { 0, 2, 3, 7 }, Measures, iso));tetrahedra.Add(new Tetrahedron(ABox, new int[] { 0, 2, 6, 7 }, Measures, iso));
    //    tetrahedra.Add(new Tetrahedron(ABox, new int[] { 0, 4, 6, 7 }, Measures, iso));tetrahedra.Add(new Tetrahedron(ABox, new int[] { 0, 6, 1, 2 }, Measures, iso));
    //    tetrahedra.Add(new Tetrahedron(ABox, new int[] { 0, 6, 1, 4 }, Measures, iso));tetrahedra.Add(new Tetrahedron(ABox, new int[] { 5, 6, 1, 4 }, Measures, iso));

    Point3d[] corners = B.GetCorners();
    Point3d[] v = new Point3d[]{corners[0],corners[1],corners[3],corners[2],corners[4],corners[5],corners[7],corners[6]};

    C = v;

    Mesh Tet0, Tet1, Tet2, Tet3, Tet4, Tet5;
    //Tet0 = Tet1 = Tet2 = Tet3 = Tet4 = Tet5 = new Mesh();
    Tet0 = new Mesh();
    Tet1 = new Mesh();
    Tet2 = new Mesh();
    Tet3 = new Mesh();
    Tet4 = new Mesh();
    Tet5 = new Mesh();
    Tet0.Vertices.AddVertices(new Point3d[]{v[0], v[1], v[3], v[5]});
    Tet1.Vertices.AddVertices(new Point3d[]{v[0], v[3], v[4], v[5]});
    Tet2.Vertices.AddVertices(new Point3d[]{v[3], v[4], v[5], v[7]});
    Tet3.Vertices.AddVertices(new Point3d[]{v[0], v[3], v[2], v[6]});
    Tet4.Vertices.AddVertices(new Point3d[]{v[0], v[3], v[6], v[4]});
    Tet5.Vertices.AddVertices(new Point3d[]{v[3], v[6], v[4], v[7]});

    Mesh[] tetrahedrons = new Mesh[6]{Tet0, Tet1, Tet2, Tet3, Tet4, Tet5};
    foreach(Mesh tetrahedron in tetrahedrons)
    {
      tetrahedron.Faces.AddFace(0, 1, 3);
      tetrahedron.Faces.AddFace(0, 3, 2);
      tetrahedron.Faces.AddFace(1, 2, 3);
      tetrahedron.Faces.AddFace(0, 2, 1);
    }
    A = tetrahedrons;



  }
