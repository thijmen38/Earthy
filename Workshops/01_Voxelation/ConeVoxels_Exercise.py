import Rhino.Geometry as rg
import math

# Get the bounding box of the brep (Shape) in question 
# (see https://developer.rhino3d.com/api/RhinoCommon/html/Methods_T_Rhino_Geometry_Brep.htm)
Bbox = 

# Find the BoundingBox object in the documentation (see previous link to documentation website & search boundingbox) 
# & find how to get the diagonal
# Find the Width, Length and Height of the diagonal of the bounding box of the Brep
W = 
L = 
H = 

# Divide the 3 axes of the bounding box by the dimensions of the voxel. 
# This will give you a U, V, W value along the bounding box.
# Turn this value into an integer by finding the next biggest integer (rounding up)
UC = 
VC = 
WC = 

# Create a container for your distance List
pointList = 
distanceList = 

# The following statements assign the boundingbox and list of distances
# to the outputs (see the data flow after this grasshopper component)
b = Bbox
c = distanceList

# Create a base plane for all the geometry 
# https://developer.rhino3d.com/api/RhinoCommon/html/T_Rhino_Geometry_Plane.htm
# Take a look at the Constructors
bPlane = rg.Plane(Bbox.Min, rg.Vector3d.XAxis, rg.Vector3d.YAxis)

# Due to the fact that all voxels are based on the center points, we need
# to create a shift towards the voxel's center, which is half of each dimension
xShift = 
yShift = 
zShift = 

# Write a for-loop that itereates through all UC, VC, WC values of the bounding box

for i in range(0, xxx):  #Replace xxx with something
    for j in range(0, yyy): #Replace yyy with something
        for k in range(0, zzz): #Replace zzz with something
            # Create a point with consideration of the half shift at each i, j, k location
            point = bPlane.PointAt(xx, yy, zz) # change xx, yy, zz
            
            # add point to pointList (google how to do this)
            pointList. 
            
            # find the closest point on the Shape, from 'point'
            # search the documentation on how to do this
            cPoint =  
            
            # find the distance from cPoint to point
            # search the documentation on how to do this
            distance = 
            
            # check if point is inside Shape (see documentation)
            # if the point is inside the brep, give it a negative value
            if not (checkthis): # replace 'checkthis' with a statement
                distance = 
                
            # add distance to distanceList
            distanceList.
            
# Assign the pointlist to a, which is an output
a = pointList